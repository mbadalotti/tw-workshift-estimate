# Trading Works - Previsão de final de turno
Sinta-se a vontade para contribuir com uma merge request.

## Instalação do Tampermonkey
Primeiramente é necessário instalar a extensão do Tampermonkey no seu navegador, para isso, acesse seu navegador e instale a extensão Tampermonkey através da loja de extensões ou então através dos links: 

[Tampermonkey para Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=pt-BR)  
[Tampermonkey para Firefox](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)  
[Tampermonkey para Edge](https://microsoftedge.microsoft.com/addons/detail/tampermonkey/iikmkjmpaadaobahmlepeloendndfphd)  
[Tampermonkey para Opera](https://addons.opera.com/en/extensions/details/tampermonkey-beta/)
 
## Atualização (ignore para primeira instalação)  
Caso você tenha instalado/configurado a versão antiga do script, primeiramente é necessário remover essa versão e só depois instalar a nova. Caso você não tenha instalado a versão antiga do script você pode pular esses passos e ir direto ao passo de instalação e configuração. 

Para fazer esse processo, clique no ícone do Tampermonkey, após clique no ícone de “+” ao lado do nome do script “TradingWorks Time Calculation” , conforme imagem abaixo: 

![Remoção de script antigo no Tampermonkey](https://gitlab.com/mbadalotti/tw-workshift-estimate/-/raw/master/images/script_uninstall.png)

## Instalação
Ao acessar o [link do script](https://gitlab.com/mbadalotti/tw-workshift-estimate/-/raw/master/tw-workshift-estimate.user.js) o Tampermonkey irá detectar automaticamente que está sendo acessada uma página que contém um script que pode ser utilizado/instalado no Tampermonkey e a página de instalação do script no Tampermonkey será aberta, nela terá um botão para instalar o script, conforme a imagem abaixo: 

![Instalação de script no Tampermonkey](https://gitlab.com/mbadalotti/tw-workshift-estimate/-/raw/master/images/script_install.png)

Pronto, a partir de agora, quando você acessar o TradingWorks  aparecerá uma coluna ao lado dos registros de ponto, denominada “Previsão de Término”, com os horários calculados de forma automática, tanto para jornadas de 8:00 diárias, quanto para jornadas de 8:48 diárias.  

Os registros de ponto ao longo do dia aparecerão conforme imagens abaixo: 


#### Turno 1 Início
![Início do Turno 1](https://gitlab.com/mbadalotti/tw-workshift-estimate/-/raw/master/images/workshift_1_start.png)

### Turno 1 Fim
![Fim do Turno 1](https://gitlab.com/mbadalotti/tw-workshift-estimate/-/raw/master/images/workshift_1_end.png)

#### Turno 2 Início
Aqui é onde a estimativa de fim de turno é feita.

![Início do Turno 2](https://gitlab.com/mbadalotti/tw-workshift-estimate/-/raw/master/images/workshift_2_start.png)

### Turno 2 Fim
![Fim do Turno 2](https://gitlab.com/mbadalotti/tw-workshift-estimate/-/raw/master/images/workshift_2_end.png)

Autores:  
[Mateus Badalotti](https://gitlab.com/mbadalotti)  
[Willian Bampi](https://gitlab.com/willianbampi)  
