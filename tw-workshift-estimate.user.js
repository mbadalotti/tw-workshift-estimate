// ==UserScript==
// @name         TradingWorks Time Calculator
// @namespace    https://app.tradingworks.net/
// @version      0.1
// @description  Add estimates to the end of the work shift
// @author       Mateus Badalotti, Willian Bampi
// @match        https://app.tradingworks.net/*
// @grant        none
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js
// @run-at       document-end
// ==/UserScript==
(function() {
	/* Check if it is point record page */
    if ($('.tw-calendar-icon').length > 0) {
        /* Fix and improve layout */
        $('table.table.table-condensed thead tr:last-of-type').append("<th class='text-center' style='padding: 2px; white-space: nowrap;'>Previsão de Término</th>");
        $('.container .row .col-sm-4:first-of-type').removeClass('col-sm-4').addClass('col-sm-8 col-md-offset-2');
        $('table.table.table-condensed tr:first-of-type th').attr('colspan', '5');
        $('#Body_Body_pnlAttendance tbody > tr .text-left').removeClass('text-left').addClass('text-center times');
        $('.col-sm-4').remove();

        /* Get times and do the calculations */
        let times800 = document.querySelectorAll('#Body_Body_pnlAttendance tbody > tr .text-center.times');
        let times848 = document.querySelectorAll('#Body_Body_pnlAttendance tbody > tr .text-center.times');
        let exitTime800;
        let exitTime848;

        if (times800.length > 1) {
            let timeArr = [];
            times800.forEach(function(element) {
                let elementText = element.textContent.trim();
                if (elementText && elementText != '__:__') {
                    timeArr.push(moment(elementText, 'HH:mm'));
                }
            });

            if (timeArr.length === 3) {
                let totalWorked = moment.duration(480, "m").subtract(moment.duration(timeArr[1].diff(timeArr[0])));
                exitTime800 = timeArr[2].add(totalWorked);
            }
        }

        if (times848.length > 1) {
            let timeArr = [];
            times848.forEach(function(element) {
                let elementText = element.textContent.trim();
                if (elementText && elementText != '__:__') {
                    timeArr.push(moment(elementText, 'HH:mm'));
                }
            });

            if (timeArr.length === 3) {
                let totalWorked = moment.duration(528, "m").subtract(moment.duration(timeArr[1].diff(timeArr[0])));
                exitTime848 = timeArr[2].add(totalWorked);
            }
        }

        if (exitTime800 && exitTime848) {
            // Append the estimated workshift end to the table
            $('table.table-condensed tbody tr:first-of-type').append("<td class='text-center' style='white-space: nowrap'><b>-</b></td>");
            $('table.table-condensed tbody tr:last-of-type').append(`<td class='text-center' style='white-space: nowrap'><strong>8h00:</strong> ${exitTime800.format('HH:mm')} <strong>|</strong> <strong>8h48:</strong> ${exitTime848.format('HH:mm')}  </td>`);
        } else {
            // The user is not in the second workshift, just append empty workshift estimate end to the table
            $('table.table-condensed tbody tr').append("<td class='text-center' style='white-space: nowrap'><b>-</b></td>");
        }
    }
})();
